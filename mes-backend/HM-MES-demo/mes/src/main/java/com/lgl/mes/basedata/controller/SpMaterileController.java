package com.lgl.mes.basedata.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lgl.mes.basedata.entity.SpMaterile;
import com.lgl.mes.basedata.entity.SpTableManager;
import com.lgl.mes.basedata.request.spMaterileReq;
import com.lgl.mes.basedata.service.ISpMaterileService;
import com.lgl.mes.common.BaseController;
import com.lgl.mes.common.Result;
import com.lgl.mes.common.util.MaterialExcelUtil;
import com.lgl.mes.common.util.log.SysLog;
import com.lgl.mes.common.util.log.SysLogAspect;
import com.lgl.mes.technology.entity.SpFlow;
import com.lgl.mes.technology.service.ISpFlowService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 物料控制器
 * </p>
 *
 * @author lgl
 * @since 2020-03-19
 */
@Controller
@RequestMapping("/basedata/materile")
public class SpMaterileController extends BaseController {

    /**
     * 物料服务
     * @date 2020-07-07
     */
    @Autowired
    private ISpMaterileService iSpMaterileService;
    /**
     * 流程服务
     */
    @Autowired
    private ISpFlowService iSpFlowService;

    @Autowired
    SysLogAspect SysLogAspect;

    /**
     * 物料管理界面
     *
     * @param model 模型
     * @return 物料管理界面
     */
    @ApiOperation("物料管理界面UI")
    @ApiImplicitParams({@ApiImplicitParam(name = "model", value = "模型", defaultValue = "模型")})
    @GetMapping("/list-ui")
    public String listUI(Model model) {
        return "basedata/materile/list";
    }


    /**
     * 物料管理修改界面
     *
     * @param model  模型
     * @param record 平台表对象
     * @return 更改界面
     */
    @ApiOperation("物料管理修改界面")
    @GetMapping("/add-or-update-ui")
    public String addOrUpdateUI(Model model, SpTableManager record) {
        if (StringUtils.isNotEmpty(record.getId())) {
            SpMaterile SpMaterile = iSpMaterileService.getById(record.getId());
            model.addAttribute("result", SpMaterile);
        }
        return "basedata/materile/addOrUpdate";
    }


    /**
     * 物料管理界面分页查询
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @ApiOperation("物料管理界面分页查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "请求参数", defaultValue = "请求参数")})
    @PostMapping("/page")
    @ResponseBody
    public Result page(spMaterileReq req) {
        QueryWrapper queryWrapper =new QueryWrapper();
        if (StringUtils.isNotEmpty(req.getMaterielLike()))
        {
            queryWrapper.like("materiel",req.getMaterielLike());
        }
        if (StringUtils.isNotEmpty(req.getMaterielDescLike()))
        {
            queryWrapper.like("materiel_desc",req.getMaterielDescLike());
        }

        if (StringUtils.isNotEmpty(req.getBatchNoLike()))
        {
            queryWrapper.like("batch_no",req.getBatchNoLike());
        }


        if (StringUtils.isNotEmpty(req.getQrcodeLike()))
        {
            queryWrapper.like("qrcode",req.getQrcodeLike());
        }



        IPage result = iSpMaterileService.page(req,queryWrapper);
        return Result.success(result);
    }

    /**
     * 物料管理修改、新增
     *
     * @param record 物料实体类
     * @return 执行结果
     */
    @SysLog("物料增加和修改")
    @ApiOperation("物料管理修改、新增")
    @PostMapping("/add-or-update")
    @ResponseBody
    public Result addOrUpdate(SpMaterile record) {
        SpFlow spflow = iSpFlowService.getById(record.getFlowId());
        if (spflow!=null)
            record.setFlowId(spflow.getFlow());
        iSpMaterileService.saveOrUpdate(record);
        return Result.success();
    }


    /**
     * 删除物料信息
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @ApiOperation("删除物料信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "物料实体", defaultValue = "物料实体")})
    @PostMapping("/delete")
    @ResponseBody
    public Result deleteByTableNameId(SpMaterile req) throws Exception {
        iSpMaterileService.removeById(req.getId());

        return Result.success();
    }

    @ApiOperation("批量删除物料信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "物料实体", defaultValue = "物料实体")})
    @PostMapping("/batchDelete")
    @ResponseBody
    public Result batchDelete( String  ids) throws Exception {

        String[] tempList = ids.split(",");
        for (String id  :  tempList)
        {
            iSpMaterileService.removeById(id);
        }

        //iSpMaterileService.removeByIds(list);

        return Result.success();
    }



    @PostMapping("/batchImport0")
    public Result  excelExport0(HttpServletRequest request, HttpServletResponse response, @RequestParam(value="file",required = false) MultipartFile file){
        long startTime = System.currentTimeMillis();
        List<String> list = new ArrayList();
        Map<String,Object> res = new HashMap<>();
        int row=1;
        int rowSuccess=0;
        Integer errorCount=0;
        QueryWrapper<SpMaterile> wrapper;
        List<SpMaterile> spMaterileList = MaterialExcelUtil.getExcelInfo(file);
        for (SpMaterile spMaterileInfo : spMaterileList) {

            wrapper=new QueryWrapper<>();
            row++;

            wrapper.eq("materiel", spMaterileInfo.getMateriel());

            List<SpMaterile> spMaterileListTemp = iSpMaterileService.list(wrapper);

            if (spMaterileListTemp !=null && spMaterileListTemp.size()>0){
                errorCount++;
            }else {
                boolean save = iSpMaterileService.saveOrUpdate(spMaterileInfo);
                if (save){
                    rowSuccess++;
                }
            }
        }

        return  Result.success( errorCount) ;
    }


    /*
       请使用  xlsx 格式

       js  文件    替换了 “请求上传接口出现异常"  --->  上传完毕

     */

    @SysLog("批量导入物料")
    @PostMapping("/batchImport")
    public Result  excelExport(@RequestParam MultipartFile file){

        String resMsg ="";

        try {
             resMsg = iSpMaterileService.LocalImport(file);
            //if (resMsg.isEmpty() || resMsg.equals(""))
               // resMsg = "导入成功";
        }
         catch (Exception e) {
             resMsg = e.getMessage();
             e.printStackTrace();
        }

        SysLogAspect.saveSysLog("批量导入物料","batchImport", file.getOriginalFilename(), resMsg);
        return  Result.success( resMsg) ;
    }

}
