package com.lgl.mes.setting.service;

import com.lgl.mes.setting.entity.SpGlobalSetting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 线体表 服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-29
 */
public interface ISpGlobalSettingService extends IService<SpGlobalSetting> {


}
