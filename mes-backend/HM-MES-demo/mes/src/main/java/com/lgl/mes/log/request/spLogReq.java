package com.lgl.mes.log.request;

import com.lgl.mes.common.BasePageReq;

/**
 * 分页对象
 * @author lgl  75039960@qq.com
 * @since 2022/08/01
 */
public class spLogReq extends BasePageReq {

    /**
     *模糊查询product
     */
    private String operationLike;
    public String getOperationLike() {
        return this.operationLike;
    }
    public void setOperationLike(String operationLike)  { this.operationLike= operationLike;}


    private String methodLike;
    public String getMethodLike() {
        return this.methodLike;
    }
    public void setMethodLike(String methodLike)  { this.methodLike= methodLike;}



    private String usernameLike;
    public String getUsernameLike() {
        return this.usernameLike;
    }
    public void setUsernameLike(String usernameLike)  { this.usernameLike= usernameLike;}




}
