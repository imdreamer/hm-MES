package com.lgl.mes.group.mapper;

import com.lgl.mes.group.entity.SpGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 工序表 Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
public interface SpGroupMapper extends BaseMapper<SpGroup> {

}
