package com.lgl.mes.setting.mapper;

import com.lgl.mes.setting.entity.SpGlobalSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 线体表 Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-29
 */
public interface SpGlobalSettingMapper extends BaseMapper<SpGlobalSetting> {

}
