package com.lgl.mes.trace.entity;

import com.alibaba.fastjson.JSONObject;
//import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lgl.mes.common.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-27
 */
@ApiModel(value="SpMaterialTrace对象", description="")
public class SpMaterialTrace extends BaseEntity{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品编码")
    private String productId;

    @ApiModelProperty(value = "产品序列号")
    private String productSerial;

    @ApiModelProperty(value = "产品名称")
    private String name;

    @ApiModelProperty(value = "bom code")
    private String bomCode;



    @ApiModelProperty(value = "产品规格")
    private String type;


    @ApiModelProperty(value = "产品二维码")
    private String qrcode;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    @ApiModelProperty(value = "产品品质: 0 合格，1 不合格 ")
    private String quality;

    @ApiModelProperty(value = "不良品工位号")
    private Integer badPos;

    @ApiModelProperty(value = "产品库位： 0， 车间，1 成品仓库，2 出库")
    private Integer position;

    @ApiModelProperty(value = "物料编码")
    private String material;

    @ApiModelProperty(value = "物料描述")
    private String materialDesc;

    @ApiModelProperty(value = "物料批次")
    private String materialBatchNo;

    public SpMaterialTrace() {
        super();
    }



    public SpMaterialTrace(JSONObject jsonObject) {

        this.material = jsonObject.getString("material");
        this.materialDesc = jsonObject.getString("materialDesc");
        this.materialBatchNo = jsonObject.getString("materialBatchNo");
        this.badPos = jsonObject.getInteger("badPos");
        this.bomCode = jsonObject.getString("bomCode");
        this.name = jsonObject.getString("name");
        this.orderNo = jsonObject.getString("orderNo");
        this.qrcode = jsonObject.getString("qrcode");
        this.position = jsonObject.getInteger("position");
        this.productSerial = jsonObject.getString("productSerial");
        this.productId = jsonObject.getString("productId");
        this.quality = jsonObject.getString("quality");
        this.type = jsonObject.getString("type");

    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }


    public String getProductSerial() {
        return productSerial;
    }

    public void setProductSerial(String productSerial) {
        this.productSerial = productSerial;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getBomCode() {
        return bomCode;
    }

    public void setBomCode(String bomCode) {
        this.bomCode = bomCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }
    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }
    public Integer getBadPos() {
        return badPos;
    }

    public void setBadPos(Integer badPos) {
        this.badPos = badPos;
    }
    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }
    public String getMaterialDesc() {
        return materialDesc;
    }

    public void setMaterialDesc(String materialDesc) {
        this.materialDesc = materialDesc;
    }
    public String getMaterialBatchNo() {
        return materialBatchNo;
    }

    public void setMaterialBatchNo(String materialBatchNo) {
        this.materialBatchNo = materialBatchNo;
    }

    //@Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "SpMaterialTrace{" +
            "productId=" + productId +
            ", name=" + name +
            ", bomCode=" + bomCode +
                ", type=" + type +
            ", qrcode=" + qrcode +
            ", orderNo=" + orderNo +
            ", quality=" + quality +
            ", badPos=" + badPos +
            ", position=" + position +
            ", material=" + material +
                ", productSerial=" + productSerial +
            ", materialDesc=" + materialDesc +
            ", materialBatchNo=" + materialBatchNo +
        "}";
    }
}

