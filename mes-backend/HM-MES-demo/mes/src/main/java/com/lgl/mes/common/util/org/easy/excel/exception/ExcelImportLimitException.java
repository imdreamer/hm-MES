package com.lgl.mes.common.util.org.easy.excel.exception;

/**
 * Excel 导入限制异常,当用户导入的条数大于限制的条数时
 * @author lisuo
 *
 */
public class ExcelImportLimitException extends ExcelException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -508353367091774340L;
	
	/**
	 * 限制条数
	 */
	private int limit;

	public ExcelImportLimitException(String message,int limit) {
		super(message);
		this.limit = limit;
	}

	public int getLimit() {
		return limit;
	}
	
}
