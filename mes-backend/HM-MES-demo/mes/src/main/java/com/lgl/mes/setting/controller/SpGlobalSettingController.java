package com.lgl.mes.setting.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import com.lgl.mes.common.BaseController;

/**
 * <p>
 * 线体表 前端控制器
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-29
 */
@Controller
@RequestMapping("/setting/sp-global-setting")
public class SpGlobalSettingController extends BaseController {

}
