package com.lgl.mes.log.mapper;

import com.lgl.mes.log.entity.SpSysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 日志  Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-09
 */
public interface SpSysLogMapper extends BaseMapper<SpSysLog> {

}
