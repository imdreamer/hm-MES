package com.lgl.mes.protocol.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import com.lgl.mes.common.BaseController;

/**
 * <p>
 * 线体表 前端控制器
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
@Controller
@RequestMapping("/protocol")
public class SpComProtocolController extends BaseController {

}
