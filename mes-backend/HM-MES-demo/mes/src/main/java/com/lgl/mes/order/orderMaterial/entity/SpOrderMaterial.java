package com.lgl.mes.order.orderMaterial.entity;

import com.alibaba.fastjson.JSONObject;
import com.lgl.mes.common.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 线体表
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-10-01
 */
@ApiModel(value="SpOrderMaterial对象", description="订单物料计划表")
public class SpOrderMaterial extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "工单编码")
    private String orderCode;

    @ApiModelProperty(value = "BOM编码")
    private String bomCode;

    @ApiModelProperty(value = "生产线")
    private String line;

    @ApiModelProperty(value = "物料编码")
    private String materialCode;

    @ApiModelProperty(value = "数量")
    private Integer amount;

    @ApiModelProperty(value = "数量")
    private Integer realAmount;

    @ApiModelProperty(value = "数量")
    private Integer remainAmount;



    @ApiModelProperty(value = "批次")
    private String batchNo;

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
    public String getBomCode() {
        return bomCode;
    }

    public void setBomCode(String bomCode) {
        this.bomCode = bomCode;
    }
    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }
    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    //@Override
    protected Serializable pkVal() {
        return null;
    }


    public Integer getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Integer realAmount) {
        this.realAmount = realAmount;
    }
    public Integer getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(Integer remainAmount) {
        this.remainAmount = remainAmount;
    }



    @Override
    public String toString() {
        return "SpOrderMaterial{" +
            "orderCode=" + orderCode +
            ", bomCode=" + bomCode +
            ", line=" + line +
            ", materialCode=" + materialCode +
            ", amount=" + amount +
                ", remainAmount=" + remainAmount +
                ", realAmount=" + realAmount +
            ", batchNo=" + batchNo +
        "}";
    }


    public SpOrderMaterial() {
        super();
    }


    public SpOrderMaterial(JSONObject jsonObject) {
        this.orderCode = jsonObject.getString("orderCode");
        this.bomCode = jsonObject.getString("bomCode");
        this.line = jsonObject.getString("line");
        this.materialCode = jsonObject.getString("materialCode");
        this.amount = jsonObject.getInteger("amount");
        this.batchNo = jsonObject.getString("batchNo");
        this.remainAmount = jsonObject.getInteger("remainAmount");
        this.realAmount = jsonObject.getInteger("realAmount");


    }

}
