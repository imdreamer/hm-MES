package com.lgl.mes.protocol.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lgl.mes.common.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 线体表
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
@ApiModel(value="SpComProtocol对象", description="线体表")
public class SpComProtocol extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "协议名称")
    private String protocol;

    @ApiModelProperty(value = "协议描述")
    private String protocolDesc;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
    public String getProtocolDesc() {
        return protocolDesc;
    }

    public void setProtocolDesc(String protocolDesc) {
        this.protocolDesc = protocolDesc;
    }

    //@Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "SpComProtocol{" +
            "protocol=" + protocol +
            ", protocolDesc=" + protocolDesc +
        "}";
    }
}
