package com.lgl.mes.employee.service.impl;

import com.lgl.mes.employee.entity.SpEmployee;
import com.lgl.mes.employee.mapper.SpEmployeeMapper;
import com.lgl.mes.employee.service.ISpEmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 工序表 服务实现类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-28
 */
@Service
public class SpEmployeeServiceImpl extends ServiceImpl<SpEmployeeMapper, SpEmployee> implements ISpEmployeeService {

}
