package com.lgl.mes.trace.entity;


import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Excel("产品溯源-产品-物料表")
public class MaterialExcel {

    /*@ExcelField(value = "发薪日期",width = 100, readConverter = ReadsConverter.class)
    private Date payDate;
    */

    @ExcelField(value = "产品编码")
    private String productId;

    @ExcelField(value = "产品序列号")
    private String productSerial;

    @ExcelField(value = "BOM编码")
    private String bomCode;

    @ExcelField(value = "物料编码")
    private String material;


    @ExcelField(value = "物料批次")
    private String materialBatchNo;

    @ExcelField(value = "产品规格")
    private String type;


/*
    @ExcelField(value = "产品二维码")
    private String qrcode;
*/

/*

    @ExcelField(value = "产品品质")  //: 0 合格，1 不合格
    private String quality;

    @ExcelField(value = "产品工位")
    private Integer badPos;
*/

/*
    @ExcelField(value = "产品库位： 0， 车间，1 成品仓库，2 出库")
    private Integer position;
*/


    @ExcelField(value = "订单编号")
    private String orderNo;

}
