package com.lgl.mes.order.orderMaterial.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lgl.mes.basedata.entity.SpTableManager;
import com.lgl.mes.common.Result;
import com.lgl.mes.common.util.log.SysLogAspect;
import com.lgl.mes.order.orderMaterial.entity.SpOrderMaterial;
import com.lgl.mes.order.orderMaterial.request.spOrderMaterialReq;
import com.lgl.mes.order.orderMaterial.service.ISpOrderMaterialService;
import com.lgl.mes.trace.entity.SpMaterialTrace;
import com.lgl.mes.trace.request.spTraceReq;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import com.lgl.mes.common.BaseController;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 * 线体表 前端控制器
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-10-01
 */
@Controller
@RequestMapping("/order/orderMaterial")
public class SpOrderMaterialController extends BaseController {

    @Autowired
    public ISpOrderMaterialService iSpOrderMaterialService ;

    @Autowired
    SysLogAspect SysLogAspect ;

    @ApiOperation("列表界面UI")
    @ApiImplicitParams({@ApiImplicitParam(name = "model", value = "模型", defaultValue = "模型")})
    @GetMapping("/list-ui")
    public String listUI(Model model) {

        return "order/orderMaterial/list";
    }


    /**
     * 物料管理修改界面
     *
     * @param record 平台表对象
     * @return 更改界面
     */
    @ApiOperation("修改界面")
    @GetMapping("/add-or-update-ui")
    public String addOrUpdateUI(Model model, SpTableManager record) {
        if (StringUtils.isNotEmpty(record.getId())) {
            SpOrderMaterial spOrderMaterial = iSpOrderMaterialService.getById(record.getId());
            model.addAttribute("result", spOrderMaterial);
        }
        return "order/orderMaterial/addOrUpdate";
    }



    /**
     * 产品管理界面分页查询
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @ApiOperation("分页查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "请求参数", defaultValue = "请求参数")})
    @PostMapping("/page")
    @ResponseBody
    public Result page(spOrderMaterialReq req) {
        QueryWrapper<SpOrderMaterial> queryWrapper =new QueryWrapper();
        //QueryWrapper queryWrapper =new QueryWrapper();
        if (StringUtils.isNotEmpty(req.getLineLike()))
        {
            queryWrapper.like("line",req.getLineLike());
        }

        if (StringUtils.isNotEmpty(req.getOrderCodeLike()))
        {
            queryWrapper.like("order_code",req.getOrderCodeLike());
        }

        // 物料编码
        if (StringUtils.isNotEmpty(req.getMaterialCodeLike()))
        {
            queryWrapper.like("material_code",req.getMaterialCodeLike());
        }

        if (StringUtils.isNotEmpty(req.getMaterialBatchNoLike()))
        {
            queryWrapper.like("batch_no",req.getMaterialBatchNoLike());
        }

        if (StringUtils.isNotEmpty(req.getBomCodeLike()))
        {
            queryWrapper.like("bom_code",req.getBomCodeLike());
        }

        IPage result = iSpOrderMaterialService.page(req,queryWrapper);
        return Result.success(result);
    }



    @ApiOperation("修改、新增")
    @PostMapping("/add-or-update")
    @ResponseBody
    public Result addOrUpdate(SpOrderMaterial record) {

        if(record==null || record.getOrderCode()==null ||record.getOrderCode().isEmpty())
        {
            return Result.failure("没有数据！");
        }

        iSpOrderMaterialService.saveOrUpdate(record);

        return Result.success();
    }


    /**
     * 删除产品信息
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @ApiOperation("删除信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "产品实体", defaultValue = "产品实体")})
    @PostMapping("/delete")
    @ResponseBody
    public Result deleteByTableNameId(SpOrderMaterial req) throws Exception {
        iSpOrderMaterialService.removeById(req.getId());
        return Result.success();
    }



    @ApiOperation("批量删除信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "ids", value = "product ids", defaultValue = "product ids")})
    @PostMapping("/batchDelete")
    @ResponseBody
    public Result batchDelete( String  ids) throws Exception {

        String[] tempList = ids.split(",");
        for (String id  :  tempList)
        {
            iSpOrderMaterialService.removeById(id);
        }

        return Result.success();
    }

    /*
  请使用  xlsx 格式

  js  文件    替换了 “请求上传接口出现异常"  --->  上传完毕

*/
    @PostMapping("/batchImport")
    public Result  excelExport(@RequestParam MultipartFile file){

        String resMsg ="";

        try {
            resMsg = iSpOrderMaterialService.LocalImport(file);
            if (resMsg.isEmpty() || resMsg.equals(""))
                resMsg = "导入成功";
        }
        catch (Exception e) {
            resMsg = e.getMessage();
            e.printStackTrace();
        }

        SysLogAspect.saveSysLog("批量订单物料计划","batchImport", file.getOriginalFilename(), resMsg);
        return  Result.success( resMsg) ;
    }


}
