package com.lgl.mes.workReport.service;

import com.lgl.mes.product.entity.SpProduct;
import com.lgl.mes.workReport.entity.SpWorkReport;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2024-08-10
 */
public interface ISpWorkReportService extends IService<SpWorkReport> {

    public boolean  UpdateOrderAndPlan (String planCode);

}
