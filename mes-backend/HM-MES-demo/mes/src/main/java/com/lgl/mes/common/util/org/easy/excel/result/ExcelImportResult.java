package com.lgl.mes.common.util.org.easy.excel.result;

import org.apache.commons.collections4.CollectionUtils;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelDataException;

import java.util.ArrayList;
import java.util.List;

/**
 * Excel导入结果
 * 
 * @author lisuo
 *
 */
@SuppressWarnings("unchecked")
public class ExcelImportResult {
	
	/** 头信息,标题行之前的数据,每行表示一个List<Object>,每个Object存放一个cell单元的值 */
	private List<List<Object>> header = null;

	/** JavaBean集合(全量,包含校验不通过的数据) */
	private List<?> listBean;
	
	/** JavaBean集合(非全量,只包含校验通过的数据)  */
	private List<?> validListBean;
	
	/** JavaBean集合(非全量,只包含校验为通过的数据)  */
	private List<?> errorListBean = new ArrayList<>();
	
	/** Errors */
	private List<ExcelDataException> errors = new ArrayList<>();
	
	/** Excel中需要处理的数据量,假设10条数据,8条校验未通过,这个值是10,listBean是2条数据 */
	private Integer totalNum;
	
	public List<List<Object>> getHeader() {
		return header;
	}

	public void setHeader(List<List<Object>> header) {
		this.header = header;
	}
	
	public <T> List<T> getListBean() {
		return (List<T>) listBean;
	}

	public void setListBean(List<?> listBean) {
		this.listBean = listBean;
	}
	
	public List<ExcelDataException> getErrors() {
		return errors;
	}
	
	public Integer getTotalNum() {
		return totalNum;
	}
	
	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}
	
	/**
	 * 导入是否含有错误
	 * @return true:有错误,false:没有错误
	 */
	public boolean hasErrors(){
		return CollectionUtils.isNotEmpty(errors);
	}
	
	public <T> List<T> getValidListBean() {
		return (List<T>)validListBean;
	}
	
	public void setValidListBean(List<?> validListBean) {
		this.validListBean = validListBean;
	}
	
	public <T> List<T> getErrorListBean() {
		return (List<T>)errorListBean;
	}
	
}
