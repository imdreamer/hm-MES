package com.lgl.mes.common.util.org.easy.excel.aop.cell.inner;

import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellInterceptorAdapter;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellJoinPoint;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelException;

import java.math.BigDecimal;

/**
 * 处理regex标签(正则)
 * @author lisuo
 *
 */
public class RegexCellInterceptor extends CellInterceptorAdapter {

	@Override
	public boolean supports(CellJoinPoint joinPoint) {
		return joinPoint.getFieldValue().getRegex() != null && joinPoint.getValue() != null;
	}

	@Override
	public Object executeImport(CellJoinPoint joinPoint) throws ExcelException {
		String val;
		if(joinPoint.getValue() instanceof Double) {
			val = doubleToString((Double)joinPoint.getValue());
		}else {
			val = String.valueOf(joinPoint.getValue());
		}
		if (!val.matches(joinPoint.getFieldValue().getRegex())) {
			String msg = joinPoint.getFieldValue().getRegexErrMsg();
			throw newExcelDataException(msg == null ? "格式错误" : msg, joinPoint);
		}
		return super.executeImport(joinPoint);
	}

	private String doubleToString(Double val) {
		return new BigDecimal(String.valueOf(val)).stripTrailingZeros().toPlainString();
	}

}
