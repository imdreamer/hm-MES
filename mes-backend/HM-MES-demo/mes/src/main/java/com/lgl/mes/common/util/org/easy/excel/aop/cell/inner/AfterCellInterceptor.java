package com.lgl.mes.common.util.org.easy.excel.aop.cell.inner;

import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellInterceptorAdapter;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellJoinPoint;
import com.lgl.mes.common.util.org.easy.excel.config.FieldValue;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelException;
import org.springframework.beans.AbstractPropertyAccessor;

/**
 * 处理after标签
 * @author lisuo
 *
 */
public class AfterCellInterceptor extends CellInterceptorAdapter {

	@Override
	public boolean supports(CellJoinPoint joinPoint) {
		return joinPoint.getFieldValue().getComputeName()!=null && joinPoint.getValue()!=null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object executeImport(CellJoinPoint joinPoint) throws ExcelException {
		AbstractPropertyAccessor accessor = joinPoint.getAccessor();
		String after = joinPoint.getFieldValue().getComputeName();
		Object afterValue = accessor.getPropertyValue(after);
		if(afterValue!=null) {
			Comparable temp = (Comparable) joinPoint.getValue();
			if(temp.compareTo(afterValue) != 1) {
				accessor.setPropertyValue(joinPoint.getFieldValue().getName(), temp);
				FieldValue computeField = joinPoint.getExcelDefinition().findByFieldName(after);
				throw newExcelDataException("不能在["+computeField.getAlias()+"]之前", joinPoint);
			}
		}
		return super.executeImport(joinPoint);
	}

}
