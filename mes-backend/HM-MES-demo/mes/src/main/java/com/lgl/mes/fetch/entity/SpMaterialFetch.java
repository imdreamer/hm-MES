package com.lgl.mes.fetch.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lgl.mes.common.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 基础物料表
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-12-10
 */
@ApiModel(value="SpMaterialFetch对象", description="基础物料表")
public class SpMaterialFetch extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "物料编码")
    private String material;

    @ApiModelProperty(value = "批次")
    private String batchNo;

    @ApiModelProperty(value = "数量")
    private Integer amount;

    @ApiModelProperty(value = "物料单位")
    private String unit;

    @ApiModelProperty(value = "领料人")
    private String handler;

    @ApiModelProperty(value = "出库仓")
    private String houseOut;


    @ApiModelProperty(value = "入库仓")
    private String houseIn;


    @ApiModelProperty(value = "逻辑删除：1 表示删除，0 表示未删除，2 表示禁用")
    private String isDeleted;


    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getHouseOut() {
        return houseOut;
    }

    public void setHouseOut(String houseOut) {
        this.houseOut = houseOut;
    }

    public String getHouseIn() {
        return houseIn;
    }

    public void setHouseIn(String houseIn) {
        this.houseIn = houseIn;
    }
    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }
    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    //@Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "SpMaterialFetch{" +
            "material=" + material +
            ", batchNo=" + batchNo +
            ", amount=" + amount +
            ", unit=" + unit +
            ", handler=" + handler +
            ", isDeleted=" + isDeleted +
                ", houseOut=" + houseOut +
                ", houseIn=" + houseIn +
        "}";
    }
}
