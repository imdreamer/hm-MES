package com.lgl.mes.employee.mapper;

import com.lgl.mes.employee.entity.SpEmployee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 工序表 Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-28
 */
public interface SpEmployeeMapper extends BaseMapper<SpEmployee> {

}
