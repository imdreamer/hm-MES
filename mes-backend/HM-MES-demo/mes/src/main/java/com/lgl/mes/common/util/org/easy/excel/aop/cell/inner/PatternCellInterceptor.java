package com.lgl.mes.common.util.org.easy.excel.aop.cell.inner;

import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellInterceptorAdapter;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellJoinPoint;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelException;
import com.lgl.mes.common.util.org.easy.excel.util.FastDateUtil;

import java.util.Date;

/**
 * 处理pattern标签(日期)
 * @author lisuo
 *
 */
public class PatternCellInterceptor extends CellInterceptorAdapter {

	@Override
	public boolean supports(CellJoinPoint joinPoint) {
		return joinPoint.getFieldValue().getPattern() != null && joinPoint.getValue() != null;
	}

	@Override
	public Object executeImport(CellJoinPoint joinPoint) throws ExcelException {
		if(joinPoint.getValue() instanceof Date) {
			return joinPoint.getValue();
		}
		String str = joinPoint.getValue().toString();
		Date date = FastDateUtil.parse(str, joinPoint.getFieldValue().getPatternArray());
		if (date == null) {
			throw newExcelDataException("[" + str + "]不能转换成日期,正确的格式应该是[" + joinPoint.getFieldValue().getPattern() + "]", joinPoint);
		}
		return date;
	}

	@Override
	public Object executeExport(CellJoinPoint joinPoint) throws ExcelException {
		if(joinPoint.getValue() instanceof Date) {
			return FastDateUtil.format((Date) joinPoint.getValue(), joinPoint.getFieldValue().getPatternArray()[0]);
		}
		return super.executeExport(joinPoint);
	}

}
