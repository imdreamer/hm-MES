package com.lgl.mes.fetch.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lgl.mes.common.Result;
import com.lgl.mes.common.util.log.SysLog;
import com.lgl.mes.device.entity.SpDevice;
import com.lgl.mes.fetch.entity.SpMaterialFetch;
import com.lgl.mes.fetch.request.spFetchReq;
import com.lgl.mes.fetch.service.ISpMaterialFetchService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import com.lgl.mes.common.BaseController;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 基础物料表 前端控制器
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-12-10
 */
@Controller
@RequestMapping("/material/fetch")
public class SpMaterialFetchController extends BaseController {

        @Autowired
        public ISpMaterialFetchService iSpMaterialFetchService;


        /**
         * 领料界面
         *
         * @param model 模型
         * @return 领料界面
         */
        @ApiOperation("领料UI")
        @ApiImplicitParams({@ApiImplicitParam(name = "model", value = "模型", defaultValue = "模型")})
        @GetMapping("/list-ui")
        public String listUI(Model model) {
            return "fetch/list";
        }


        /**
         *领料编辑界面
         *
         * @param model  模型
         * @param record 平台表对象
         * @return 更改界面
         */
        @ApiOperation("领料编辑界面")
        @GetMapping("/add-or-update-ui")
        public String addOrUpdateUI(Model model, SpDevice record) throws Exception {
            if (StringUtils.isNotEmpty(record.getId())) {
                SpMaterialFetch spMaterialFetch = iSpMaterialFetchService.getById(record.getId());
                model.addAttribute("result", spMaterialFetch);
            }
            return "fetch/addOrUpdate";
        }


        /**
         * 领料分页查询
         *
         * @param req 请求参数
         * @return Result 执行结果
         */
        @ApiOperation("领料信息分页查询")
        @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "请求参数", defaultValue = "请求参数")})
        @PostMapping("/page")
        @ResponseBody
        public Result page(spFetchReq req) {
            QueryWrapper<SpMaterialFetch> queryWrapper =new QueryWrapper();

            if (StringUtils.isNotEmpty(req.getMaterialLike()))
            {
                queryWrapper.like("material",req.getMaterialLike());
            }

            IPage result = iSpMaterialFetchService.page(req,queryWrapper);
            return Result.success(result);
        }


        /**
         *  领料新增+修改
         *
         * @param spMaterialFetch 领料
         * @return 执行结果
         */
        @SysLog("新增+修改")
        @ApiOperation("新增+修改")
        @PostMapping("/add-or-update")
        @ResponseBody
        public Result addOrUpdate(/*@RequestBody*/ SpMaterialFetch spMaterialFetch) throws Exception {
            iSpMaterialFetchService.saveOrUpdate(spMaterialFetch);

            return Result.success();
        }

        /**
         * 删除领料
         *
         *
         * @param req 请求参数
         * @return Result 执行结果
         */
        @SysLog("delete领料记录")
        @ApiOperation("delete领料记录")
        @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "领料实体", defaultValue = "领料实体")})
        @PostMapping("/delete")
        @ResponseBody
        public Result deleteByTableNameId(SpMaterialFetch req) throws Exception {

            iSpMaterialFetchService.removeById(req.getId());

            return Result.success();
        }

    }
