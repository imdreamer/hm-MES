package com.lgl.mes.common.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class MyControllerAdvice {

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Map errorHandler(Exception ex){
        Map map = new HashMap<>();
        map.put("msg",ex.getMessage());
        return map;
    }

    @ResponseBody
    @ExceptionHandler(value = MyException.class)
    public Map MyErrorHandler(MyException ex){
        Map map = new HashMap<>();
        map.put("msg",ex.getMessage());
        return map;
    }
}
