package com.lgl.mes.trace.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lgl.mes.common.util.org.easy.excel.ExcelContext;
import com.lgl.mes.common.util.org.easy.excel.result.ExcelImportResult;
import com.lgl.mes.technology.entity.SpBom;
import com.lgl.mes.trace.entity.MaterialExcel;
import com.lgl.mes.trace.entity.SpMaterialTrace;
import com.lgl.mes.basedata.service.impl.FileNotEmptyException;
import com.lgl.mes.trace.mapper.SpMaterialTraceMapper;
import com.lgl.mes.trace.service.ISpMaterialTraceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuwenze.poi.ExcelKit;
import com.wuwenze.poi.handler.ExcelReadHandler;
import com.wuwenze.poi.pojo.ExcelErrorField;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-27
 */
@Service
public class SpMaterialTraceServiceImpl extends ServiceImpl<SpMaterialTraceMapper, SpMaterialTrace> implements ISpMaterialTraceService {

    //@Override
    public String LocalImport_1(MultipartFile file) {
        String message = "上传完毕";
        List<MaterialExcel> successList = new ArrayList();
        List<Map<String, Object>> errorList = new ArrayList();
        if (file.isEmpty()) {
            throw new FileNotEmptyException("上传文件不能为空");
        }
        try {
            ExcelKit.$Import(MaterialExcel.class)
                    .readXlsx(file.getInputStream(), new ExcelReadHandler<MaterialExcel>() {
                        @Override
                        public void onSuccess(int i, int i1, MaterialExcel materialExcel) {
                            successList.add(materialExcel); // 单行读取成功，加入入库队列。
                        }
                        @Override
                        public void onError(int sheetIndex, int rowIndex, List<ExcelErrorField> list) {
                            // 读取数据失败，记录了当前行所有失败的数据
                            Map<String, Object> map = new HashMap<>();
                            map.put("sheetIndex", sheetIndex);
                            map.put("rowIndex", rowIndex);
                            map.put("errorFields", list);
                            errorList.add(map);
                        }
                    });


            successList.stream().forEach(item ->{
                //ese.setCreateDate(new Date());
                //.setCreateBy(UserUtil.getUserId());
                JSONObject jsonObject = (JSONObject) JSONObject.toJSON(item);
                String productId  = jsonObject.getString("productId");
                String productSerial  = jsonObject.getString("productSerial");
                String material  = jsonObject.getString("material");
                QueryWrapper<SpMaterialTrace> wrapper=new QueryWrapper<>();
                wrapper.eq("product_id", productId);
                wrapper.eq("product_serial", productSerial);
                wrapper.eq("material", material);  //and
                if (this.list(wrapper).size()==0) {   //去重
                    SpMaterialTrace spMaterialTrace = new SpMaterialTrace(jsonObject);
                    this.baseMapper.insert(spMaterialTrace);
                }
            });
        } catch (Exception e) {
            message = e.getMessage();
            e.printStackTrace();
        }
        return message;
    }



    @Override
    public String LocalImport(MultipartFile file) {
        String message = "上传完毕";
        if (file.isEmpty()) {
            throw new FileNotEmptyException("上传文件不能为空");
        }
        try
        {
            String config = "excel/mes-excel-config.xml";
            ExcelContext context = new ExcelContext(config);
            // Excel配置文件中配置的id
            String excelId = "materialTrace";

            ExcelImportResult result = context.readExcel(excelId, 0, file.getInputStream());
            List<SpMaterialTrace>  items = result.getListBean();
            for(SpMaterialTrace item:items){
                QueryWrapper<SpMaterialTrace> wrapper=new QueryWrapper<>();
                wrapper.eq("product_id", item.getProductId());
                wrapper.eq("product_serial", item.getProductSerial());
                wrapper.eq("material", item.getMaterial());
                if (this.list(wrapper).size()==0) {
                    this.baseMapper.insert(item);
                }
            }

        } catch (Exception e) {
            message = e.getMessage();
            e.printStackTrace();
        }

        return message;
    }



}
