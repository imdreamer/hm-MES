package com.lgl.mes.common.util.org.easy.excel.aop.cell;

import com.lgl.mes.common.util.org.easy.excel.exception.ExcelDataException;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelException;

public abstract class CellInterceptorAdapter implements CellInterceptor{

	@Override
	public boolean supports(CellJoinPoint joinPoint) {
		return true;
	}

	@Override
	public Object executeImport(CellJoinPoint joinPoint) throws ExcelException {
		return joinPoint.getValue();
	}

	@Override
	public Object executeExport(CellJoinPoint joinPoint) throws ExcelException {
		return joinPoint.getValue();
	}
	
	public ExcelDataException newExcelDataException(String message, CellJoinPoint joinPoint) {
		return new ExcelDataException(message, joinPoint.getRowNum(), joinPoint.getFieldValue().getAlias(), joinPoint.getOriginalValue(), joinPoint.getRefObject());
	}
	
}
