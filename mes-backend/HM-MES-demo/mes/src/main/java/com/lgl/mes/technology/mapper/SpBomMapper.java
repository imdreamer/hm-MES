package com.lgl.mes.technology.mapper;

import com.lgl.mes.product.entity.SpProduct;
import com.lgl.mes.technology.entity.SpBom;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lgl
 * @since 2020-03-28
 */
public interface SpBomMapper extends BaseMapper<SpBom> {


    @Select("select distinct bom_code from sp_bom   ")
    List<String> getBomList() ;

/*
    ;
    @Update({ "update SpProduct set productId = #{productId}   where id = #{id}" })
    int updateSpProductById(SpProduct spProduct);
*/




}
