package com.lgl.mes.basedata.service.impl;

public class FileNotEmptyException extends RuntimeException{

    public FileNotEmptyException(String message) {
        super(message);
    }
}

