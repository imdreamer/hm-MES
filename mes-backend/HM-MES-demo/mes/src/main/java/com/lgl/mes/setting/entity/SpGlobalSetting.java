package com.lgl.mes.setting.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lgl.mes.common.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 线体表
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-29
 */
@ApiModel(value="SpGlobalSetting对象", description="线体表")
public class SpGlobalSetting extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "看板URL")
    private String fronturl;


    @ApiModelProperty(value = "rabbitmq URL")
    private String rabbitmqurl;

    @ApiModelProperty(value = "生产线")
    private String line;

    public String getFronturl() {
        return fronturl;
    }
    public String getRabbitmqurl() {
        return rabbitmqurl;
    }

    public void setFronturl(String fronturl) {
        this.fronturl = fronturl;
    }

    public void setRabbitmqurl(String rabbitmqurl) {
        this.rabbitmqurl = rabbitmqurl;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    //@Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "SpGlobalSetting{" +
            "fronturl=" + fronturl +
                "rabbitmqurl=" + rabbitmqurl +
            ", line=" + line +
        "}";
    }
}
