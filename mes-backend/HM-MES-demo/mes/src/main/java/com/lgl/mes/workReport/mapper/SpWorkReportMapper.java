package com.lgl.mes.workReport.mapper;

import com.lgl.mes.workReport.entity.SpWorkReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2024-08-10
 */
public interface SpWorkReportMapper extends BaseMapper<SpWorkReport> {

}
