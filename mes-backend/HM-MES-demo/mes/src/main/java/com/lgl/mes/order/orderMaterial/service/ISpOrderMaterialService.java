package com.lgl.mes.order.orderMaterial.service;

import com.lgl.mes.order.orderMaterial.entity.SpOrderMaterial;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 线体表 服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-10-01
 */
public interface ISpOrderMaterialService extends IService<SpOrderMaterial> {
    public String LocalImport(MultipartFile file);

}
