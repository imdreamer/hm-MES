package com.lgl.mes.trace.request;

import com.lgl.mes.common.BasePageReq;

/**
 * 分页对象
 * @author lgl  75039960@qq.com
 * @since 2022/04/01
 */
public class spTraceReq extends BasePageReq {

    /**
     *模糊查询product
     */
    private String productIdLike;

    private String productSerialLike;

    private String bomCodeLike;

    private String materialLike;

    private String qualityLike;

    private String materialBatchNoLike;




    private String qrcodeLike;

    private  String  createTime1;
    private  String  createTime2;


    public String getMaterialBatchNoLike() {
        return this.materialBatchNoLike;
    }

    public String getqualityLike() {
        return this.qualityLike;
    }

    public String getMaterialLike() {
        return this.materialLike;
    }

    public String getProductIdLike() {
        return this.productIdLike;
    }

    public String getProductSerialLike() {
        return this.productSerialLike;
    }

    public String getQrcodeLike() {
        return this.qrcodeLike;
    }
    public String getBomCodeLike() {
        return this.bomCodeLike;
    }
    public String getCreateTime1() {
        return this.createTime1;
    }
    public String getCreateTime2() {
        return this.createTime2;
    }

    public void setMaterialLike(String materialLike) {
        this.materialLike = materialLike;
    }

    public void setProductIdLike(String productIdLike)  { this.productIdLike= productIdLike;}
    public void setProductSerialLike(String productSerialLike)  { this.productSerialLike= productSerialLike;}

    public void setBomCodeLike(String bomCodeLike) {
        this.bomCodeLike = bomCodeLike;
    }

    public void setQrcodeLike(String qrcodeLike) {
        this.qrcodeLike = qrcodeLike;
    }
    public void setCreateTime1(String createTime1) {
        this.createTime1 = createTime1;
    }
    public void setCreateTime2(String createTime2) {
        this.createTime2 = createTime2;
    }

    public void setQualityLike(String qualityLike) {
        this.qualityLike = qualityLike;
    }

    public void setMaterialBatchNoLike(String materialBatchNoLike) {
        this.materialBatchNoLike = materialBatchNoLike;
    }



}
