package com.lgl.mes.group.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lgl.mes.common.Result;
import com.lgl.mes.common.util.log.SysLog;
import com.lgl.mes.config.entity.SpConfig;

import com.lgl.mes.device.entity.SpDevice;
import com.lgl.mes.group.entity.SpGroup;
import com.lgl.mes.group.request.SpGroupReq;
import com.lgl.mes.group.service.ISpGroupService;


import com.lgl.mes.protocol.entity.SpComProtocol;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import com.lgl.mes.common.BaseController;

import java.util.List;

/**
 * <p>
 * 工序表 前端控制器
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
@Controller
@RequestMapping("/group")
public class SpGroupController extends BaseController {

    @Autowired
    public ISpGroupService iSpGroupService;

    /**
     * 设备管理界面
     *
     * @param model 模型
     * @return 设备管理界面
     */
    @ApiOperation("UI")
    @ApiImplicitParams({@ApiImplicitParam(name = "model", value = "模型", defaultValue = "模型")})
    @GetMapping("/list-ui")
    public String listUI(Model model) {
        return "group/list";
    }


    /**
     *config编辑界面
     *
     * @param model  模型
     * @param record 平台表对象
     * @return 更改界面
     */
    @ApiOperation("管理编辑界面")
    @GetMapping("/add-or-update-ui")
    public String addOrUpdateUI(Model model, SpDevice record) throws Exception {
        if (StringUtils.isNotEmpty(record.getId())) {
            SpGroup spGroup = iSpGroupService.getById(record.getId());
            model.addAttribute("result", spGroup);
        }
        return "group/addOrUpdate";
    }


    /**
     * 分页查询
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @ApiOperation("信息分页查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "请求参数", defaultValue = "请求参数")})
    @PostMapping("/page")
    @ResponseBody
    public Result page(SpGroupReq req) {
        QueryWrapper<SpGroup> queryWrapper =new QueryWrapper();

        if (StringUtils.isNotEmpty(req.getGroupLike()))
        {
            queryWrapper.like("name",req.getGroupLike());
        }

        IPage result = iSpGroupService.page(req,queryWrapper);
        return Result.success(result);
    }


    /**
     *
     *
     * @param spGroup 流程与工序DTO
     * @return 执行结果
     */
    @SysLog("新增/修改班组记录")
    @ApiOperation("新增+修改")
    @PostMapping("/add-or-update")
    @ResponseBody
    public Result addOrUpdate(/*@RequestBody*/ SpGroup spGroup) throws Exception {
        iSpGroupService.saveOrUpdate(spGroup);

        return Result.success();
    }

    /**
     * 删除流程
     *
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @SysLog("delete班组记录")
    @ApiOperation("delete记录")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "实体", defaultValue = "实体")})
    @PostMapping("/delete")
    @ResponseBody
    public Result deleteByTableNameId(SpConfig req) throws Exception {

        iSpGroupService.removeById(req.getId());

        return Result.success();
    }



    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public Result getGroups() throws Exception {


        List<SpGroup> list = iSpGroupService.list();

        return Result.success(list);

    }


}
