package com.lgl.mes.common.util.org.easy.excel.aop.cell;

import com.lgl.mes.common.util.org.easy.excel.exception.ExcelException;

/**
 * Cell拦截器</br>
 * 从pojo的角度这个拦截器是基于字段维度的</br>
 * 从excel角度这个拦截器是处理cell(单元格：当前行的当前列)的
 * @author lisuo
 *
 */
public interface CellInterceptor {
	
	/**
	 * 判断是否应执行executeImport方法或executeExport
	 * @param joinPoint 切入点
	 * @return true or false
	 */
	boolean supports(CellJoinPoint joinPoint);
	
	/**
	 * 导入数据执行的切面方法,可以抛出任何异常打断当前的导入行为,
	 * 如果返回的异常是ExcelDataException,并开启了多行校验那么不会打断当前的导入行为
	 * @param joinPoint 切入点
	 * @return excel的列赋值到pojo的field的value
	 * @throws ExcelException 
	 */
	Object executeImport(CellJoinPoint joinPoint) throws ExcelException;

	/**
	 * 导出数据执行切面的方法,抛出任何异常都会打断当前的导出行为
	 * @param joinPoint 切入点
	 * @return pojo的field赋值到cell的value
	 * @throws ExcelException
	 */
	Object executeExport(CellJoinPoint joinPoint) throws ExcelException;
	
}
