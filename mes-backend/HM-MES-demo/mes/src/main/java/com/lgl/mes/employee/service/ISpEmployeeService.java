package com.lgl.mes.employee.service;

import com.lgl.mes.employee.entity.SpEmployee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 工序表 服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-28
 */
public interface ISpEmployeeService extends IService<SpEmployee> {

}
