package com.lgl.mes.config.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lgl.mes.config.entity.SpConfig;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-23
 */
public interface SpConfigMapper extends BaseMapper<SpConfig> {

}
