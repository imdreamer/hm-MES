package com.lgl.mes.workReport.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lgl.mes.common.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2024-08-10
 */
@ApiModel(value="SpWorkReport对象", description="")
public class SpWorkReport extends BaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "报工单编码")
    private String reportCode;

    @ApiModelProperty(value = "日计划编码")
    private String planCode;

    @ApiModelProperty(value = "订单编码")
    private String orderCode;

    @ApiModelProperty(value = "计划日期")
    private String planDate;

    @ApiModelProperty(value = "报工时长（分钟）")
    private Integer useTime;

    @ApiModelProperty(value = "报工产量")
    private Integer makedQty;

    @ApiModelProperty(value = "次品数量")
    private Integer badQty;

    @ApiModelProperty(value = "逻辑删除：1 表示删除，0 表示未删除，2 表示禁用")
    private Integer isDeleted;

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }
    public String getOrderCode() {
        return orderCode;
    }

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }


    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }
    public Integer getUseTime() {
        return useTime;
    }

    public void setUseTime(Integer useTime) {
        this.useTime = useTime;
    }
    public Integer getMakedQty() {
        return makedQty;
    }

    public void setMakedQty(Integer makedQty) {
        this.makedQty = makedQty;
    }
    public Integer getBadQty() {
        return badQty;
    }

    public void setBadQty(Integer badQty) {
        this.badQty = badQty;
    }
    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    //@Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "SpWorkReport{" +
            "reportCode="  +reportCode +
            "planCode=" + planCode +
            ", orderCode=" + orderCode +
            ", planDate=" + planDate +
            ", useTime=" + useTime +
            ", makedQty=" + makedQty +
            ", badQty=" + badQty +
            ", isDeleted=" + isDeleted +
        "}";
    }
}
