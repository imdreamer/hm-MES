package com.lgl.mes.basedata.entity;


import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import com.lgl.mes.common.util.ReadsConverter;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
@Data
@ToString
@Excel("物料表")
public class MaterialExcel {

    /*@ExcelField(value = "发薪日期",width = 100, readConverter = ReadsConverter.class)
    private Date payDate;
    */
    /**
     * 物料编码
     */
    @ExcelField(value = "物料编码")
    private String materiel;

    /**
     * 物料描述
     */
    @ExcelField(value = "物料描述")
    private String materielDesc;


    /**
     * 物料二维码/条形码
     *
     */
    @ExcelField(value = "二维码")
    private String qrcode;

    /**
     * 批次
     */
    @ExcelField(value = "批次")
    private String batchNo;

    /**
     * 基本单位
     */
   /* @ExcelField(value = "基本单位")
    private String unit;*/

   /* *//**
     * 产品组
     *//*
    @ExcelField(value = "产品组")
    private String productGroup;
*/
    /**
     * 物料类型
     */
    @ExcelField(value = "物料类型")
    private String matType;

    /**
     * 尺寸
     */
    /*@ExcelField(value = "尺寸")
    private String size;
*/
    /**
     * 流程ID
     */
    @ExcelField(value = "流程ID")
    private String flowId;


    /**
     * 流程描述
     */
    @ExcelField(value = "流程描述")
    private String flowDesc;
}
