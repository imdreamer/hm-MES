package com.lgl.mes.order.orderMaterial.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.lgl.mes.basedata.entity.MaterialExcel;
import com.lgl.mes.basedata.entity.SpMaterile;
import com.lgl.mes.basedata.service.impl.FileNotEmptyException;
import com.lgl.mes.common.util.org.easy.excel.ExcelContext;
import com.lgl.mes.common.util.org.easy.excel.result.ExcelImportResult;
import com.lgl.mes.order.entity.OrderMaterialExcel;
import com.lgl.mes.order.orderMaterial.entity.SpOrderMaterial;
import com.lgl.mes.order.orderMaterial.mapper.SpOrderMaterialMapper;
import com.lgl.mes.order.orderMaterial.service.ISpOrderMaterialService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuwenze.poi.ExcelKit;
import com.wuwenze.poi.handler.ExcelReadHandler;
import com.wuwenze.poi.pojo.ExcelErrorField;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 线体表 服务实现类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-10-01
 */
@Service
public class SpOrderMaterialServiceImpl extends ServiceImpl<SpOrderMaterialMapper, SpOrderMaterial> implements ISpOrderMaterialService {

    //@Override
    public String LocalImport1(MultipartFile file) {
        String message = "";
        List<OrderMaterialExcel> successList = new ArrayList();
        List<Map<String, Object>> errorList = new ArrayList();
        if (file.isEmpty()) {
            throw new FileNotEmptyException("上传文件不能为空");
        }
        try {
            ExcelKit.$Import(OrderMaterialExcel.class)
                    .readXlsx(file.getInputStream(), new ExcelReadHandler<com.lgl.mes.order.entity.OrderMaterialExcel>() {
                        @Override
                        public void onSuccess(int i, int i1, OrderMaterialExcel orderMaterialExcel) {
                            successList.add(orderMaterialExcel); // 单行读取成功，加入入库队列。
                        }
                        @Override
                        public void onError(int sheetIndex, int rowIndex, List<ExcelErrorField> list) {
                            // 读取数据失败，记录了当前行所有失败的数据
                            Map<String, Object> map = new HashMap<>();
                            map.put("sheetIndex", sheetIndex);
                            map.put("rowIndex", rowIndex);
                            map.put("errorFields", list);
                            errorList.add(map);
                        }
                    });


            successList.stream().forEach(item ->{
                JSONObject jsonObject = (JSONObject) JSONObject.toJSON(item);
              /*  String materiel  = jsonObject.getString("materiel");
                QueryWrapper<SpOrderMaterial> wrapper=new QueryWrapper<>();
                wrapper.eq("materiel", materiel);
                if (this.list(wrapper).size()==0)*/
                if (true)
                {   //不去重
                    SpOrderMaterial obj = new SpOrderMaterial(jsonObject);
                    this.baseMapper.insert(obj);
                }
            });
        } catch (Exception e) {
            message = e.getMessage();
            e.printStackTrace();
        }
        return message;
    }



    @Override
    public String LocalImport(MultipartFile file) {

        String message = "上传完毕";
        if (file.isEmpty()) {
            throw new FileNotEmptyException("上传文件不能为空");
        }

        try {
            String config = "excel/mes-excel-config.xml";
            ExcelContext context = new ExcelContext(config);
            // Excel配置文件中配置的id
            String excelId = "materialPlan";

            ExcelImportResult result = context.readExcel(excelId, 0, file.getInputStream());
            List<SpOrderMaterial>  items = result.getListBean();
            for(SpOrderMaterial item:items){
                //不需要去重
               // QueryWrapper<SpOrderMaterial> wrapper=new QueryWrapper<>();
                //wrapper.eq("materiel", item.getMateriel());
                //if (this.list(wrapper).size()==0) {
                    this.baseMapper.insert(item);
                //}
            }

        } catch (Exception e) {
            message = e.getMessage();
            e.printStackTrace();
        }

        return message;
    }



}
