package com.lgl.mes.group.request;

import com.lgl.mes.common.BasePageReq;

/**
 * 分页对象
 * @author lgl  75039960@qq.com
 * @since 2022/08/01
 */
public class SpGroupReq extends BasePageReq {

    /**
     *模糊查询group
     */
    private String groupLike;


    public String getGroupLike() {
        return this.groupLike;
    }
    public void setGroupLike(String groupLike)  { this.groupLike= groupLike;}


}
