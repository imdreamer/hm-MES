package com.lgl.mes.setting.service.impl;

import com.lgl.mes.setting.entity.SpGlobalSetting;
import com.lgl.mes.setting.mapper.SpGlobalSettingMapper;
import com.lgl.mes.setting.service.ISpGlobalSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 线体表 服务实现类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-29
 */
@Service
public class SpGlobalSettingServiceImpl extends ServiceImpl<SpGlobalSettingMapper, SpGlobalSetting> implements ISpGlobalSettingService {

}
