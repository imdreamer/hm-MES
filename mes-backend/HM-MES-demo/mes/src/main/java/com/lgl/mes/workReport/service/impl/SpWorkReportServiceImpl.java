package com.lgl.mes.workReport.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lgl.mes.basedata.common.entity.SpSysDict;
import com.lgl.mes.daily.entity.SpDailyPlan;
import com.lgl.mes.daily.service.ISpDailyPlanService;
import com.lgl.mes.order.entity.SpOrder;
import com.lgl.mes.order.service.ISpOrderService;
import com.lgl.mes.workReport.entity.SpWorkReport;
import com.lgl.mes.workReport.mapper.SpWorkReportMapper;
import com.lgl.mes.workReport.service.ISpWorkReportService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2024-08-10
 */
@Service
public class SpWorkReportServiceImpl extends ServiceImpl<SpWorkReportMapper, SpWorkReport> implements ISpWorkReportService {

    @Autowired
    private ISpDailyPlanService iSpDailyPlanService;

    @Autowired
    private ISpOrderService iSpOrderService;

    @Autowired
    private ISpWorkReportService iSpWorkReportService;

    public boolean  UpdateOrderAndPlan (String planID)
    {

        QueryWrapper<SpDailyPlan> queryWrapper =new QueryWrapper();
        queryWrapper.eq("plan_code",planID);
        List<SpDailyPlan> list = iSpDailyPlanService.list(queryWrapper);

        if(list!=null  && list.size()>0) {
            SpDailyPlan spDailyPlan = list.get(0);
            QueryWrapper<SpWorkReport> queryWrapperWr =new QueryWrapper();
            queryWrapperWr.eq("plan_code",planID);
            List<SpWorkReport> listWr = iSpWorkReportService.list(queryWrapperWr);
            Integer  makedQty =0;
            Integer  badQty =0;
            Integer  useTime=0;
            for ( int i=0; i<listWr.size();i++  )
            {
                makedQty += listWr.get(i).getMakedQty();
                badQty += listWr.get(i).getBadQty();
                useTime+=listWr.get(i).getUseTime();
            }
            spDailyPlan.setMakedQty(makedQty);
            spDailyPlan.setBadQty(badQty);
            spDailyPlan.setFinishRate(spDailyPlan.getMakedQty()/spDailyPlan.getPlanQty().floatValue()*100);
            spDailyPlan.setPassRate((spDailyPlan.getMakedQty()-spDailyPlan.getBadQty())/spDailyPlan.getMakedQty().floatValue()*100);
            spDailyPlan.setRealPieceTime(useTime*60/makedQty);
            iSpDailyPlanService.saveOrUpdate(spDailyPlan);

            QueryWrapper<SpOrder> queryWrapperOrder =new QueryWrapper();
            queryWrapperOrder.eq("order_code",spDailyPlan.getOrderCode());
            List<SpOrder> lisrOrder = iSpOrderService.list(queryWrapperOrder);
            if(lisrOrder!=null  && lisrOrder.size()>0) {
                SpOrder order = lisrOrder.get(0);
                makedQty =0;
                badQty =0;

                QueryWrapper<SpWorkReport> queryWrapperWr2 =new QueryWrapper();
                queryWrapperWr2.eq("order_code",order.getOrderCode());
                List<SpWorkReport> listWr2 = iSpWorkReportService.list(queryWrapperWr2);
                for ( int i=0; i<listWr2.size();i++  )
                {
                    makedQty += listWr2.get(i).getMakedQty();
                    badQty += listWr2.get(i).getBadQty();
                }
                order.setMakedQty(makedQty);
                order.setBadQty(badQty);
                order.setFinishRate(order.getMakedQty()/order.getPlanQty().floatValue()*100);
                order.setPassRate((order.getMakedQty()-order.getBadQty())/order.getMakedQty().floatValue()*100);
                iSpOrderService.saveOrUpdate(order);
            }
        }

        return   true;
    }
}
