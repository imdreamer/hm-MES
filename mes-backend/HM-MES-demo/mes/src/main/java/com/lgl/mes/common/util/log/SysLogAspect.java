package com.lgl.mes.common.util.log;

import com.google.gson.Gson;
import com.lgl.mes.common.util.HttpContextUtils;
import com.lgl.mes.common.util.IPUtils;
import com.lgl.mes.log.entity.SpSysLog;
import com.lgl.mes.log.service.ISpSysLogService;
import com.lgl.mes.system.entity.SysUser;

import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 系统日志，切面处理类
 *
 * @author haoxin
 * @date 2021-02-02
 **/
@Aspect
@Component
public class SysLogAspect {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ISpSysLogService iSpSysLogService;
    @Pointcut("@annotation(com.lgl.mes.common.util.log.SysLog)")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long beginTime = System.currentTimeMillis();
        //执行方法
        Object result = point.proceed();
        //执行时长(毫秒)
        long time = System.currentTimeMillis() - beginTime;
        try {
            //保存日志
            saveSysLog(point, time);
        } catch (Exception e) {
            logger.error("保存日志失败："+e.getMessage());
        }

        return result;
    }

    private void saveSysLog(ProceedingJoinPoint joinPoint, long time) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        SysLog syslog = method.getAnnotation(SysLog.class);
        String operation = null;
        if(syslog != null){
            //注解上的描述
            operation = syslog.value();
        }

        //请求的方法名
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        //String methodString = className + "." + methodName + "()";  //会暴露程序，这个不能对外
        String methodString = methodName + "()";


        //请求的参数
        String params = null;
        Object[] args = joinPoint.getArgs();
        try{
            params = new Gson().toJson(args);
        } catch (Exception e){
            logger.error(e.getMessage());
        }
        //获取request
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        //设置IP地址
        String ip = IPUtils.getIpAddr(request);

        try{
            String userNameStr = ((SysUser) SecurityUtils.getSubject().getPrincipal()).getUsername();

            SpSysLog spSysLog  =  new SpSysLog();
            spSysLog.setIp(ip);
            spSysLog.setUserName(userNameStr);
            spSysLog.setOperation(operation);
            spSysLog.setMethod(methodString);
            spSysLog.setParams(params);
            spSysLog.setTime(time);

            iSpSysLogService.saveOrUpdate(spSysLog);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }



    public void saveSysLog(String methodString , String operation,String params,String msg ) {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        String ip = IPUtils.getIpAddr(request);

        try{
            String userNameStr = ((SysUser) SecurityUtils.getSubject().getPrincipal()).getUsername();

            SpSysLog spSysLog  =  new SpSysLog();
            spSysLog.setIp(ip);
            spSysLog.setUserName(userNameStr);
            spSysLog.setOperation(msg);
            spSysLog.setMethod(methodString);
            spSysLog.setParams(params);
            spSysLog.setTime(99L);

            iSpSysLogService.saveOrUpdate(spSysLog);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

}
