package com.lgl.mes.technology.entity;


import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Excel("Bom表")
public class BomExcel {
    /**
     * 物料编码
     */
    @ExcelField(name = "Bom编码"  )
    private String bomCode;


    @ExcelField(value = "物料编码")
    private String materielCode;





    /**
     * 状态
     *
     */
    //@ExcelField(value = "状态")
    //private String deleted;

    /**
     * 批次
     */
    @ExcelField(value = "生产商")
    private String factory;


    /**
     * 物料描述
     */

    @ExcelField(value = "物料名称")
    private String materielDesc;


}
