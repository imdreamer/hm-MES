<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>工单-BOM</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <#include "${request.contextPath}/common/common.ftl">
</head>
<body>
<div class="splayui-container">
    <div class="splayui-main">
        <!--查询参数-->
        <form id="js-search-form" class="layui-form" lay-filter="js-q-form-filter">
            <div class="layui-form-item">

                <div class="layui-inline">
                    <label class="layui-form-label">订单编号</label>
                    <div class="layui-input-inline">
                        <input type="orderCode" name="orderCode"  id="orderCode"  autocomplete="off" class="layui-input">
                    </div>
                </div>


                <div class="layui-inline">
                    <label or="js-status"   class="layui-form-label">订单状态</label>
                    <div class="layui-input-inline">
                        <select id="js-status" name="status"   οnchange="getvalue(this)"   lay-filter="js-status-filter" lay-verify="" >
                        </select>
                    </div>
                </div>

                <div class="layui-inline"   style="margin-left:30px; margin-right: 30px" >
                    <a class="layui-btn" lay-submit lay-filter="js-search-filter"><i
                                class="layui-icon layui-icon-search layuiadmin-button-btn"></i></a>
                </div>

                <div class="layui-inline">

                    <div class="layui-input-inline">
                        <input type="createTime" name="createTime1" autocomplete="off" class="layui-input"
                            placeholder="2022-06-20"
                               style="width:120px "  >
                    </div>
                </div>
                <div class="layui-inline"   style="margin-left:2px;">

                    <div class="layui-input-inline">
                        <input type="createTime" name="createTime2" autocomplete="off" class="layui-input"
                               placeholder="2099-12-30"
                               style="width:120px "  >
                    </div>
                </div>

            </div>
        </form>

        <!--表格-->
        <table class="layui-hide" id="js-record-table" lay-filter="js-record-table-filter"></table>
    </div>
</div>

<!--表格头操作模板-->
<script type="text/html" id="js-record-table-toolbar-top">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-danger layui-btn-sm" lay-event="deleteBatch"><i
                    class="layui-icon">&#xe640;</i>批量删除
        </button>
        <@shiro.hasPermission name="user:add">
            <button class="layui-btn layui-btn-sm" lay-event="add"><i class="layui-icon">&#xe61f;</i>添加</button>
        </@shiro.hasPermission>
    </div>
</script>

<!--行操作模板-->
<script type="text/html" id="js-record-table-toolbar-right">
    <a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete"><i class="layui-icon layui-icon-delete"></i>删除</a>
</script>

<!--js逻辑-->
<script>
    layui.use(['form', 'table', 'spLayer', 'spTable'], function () {
        var form = layui.form,
            table = layui.table,
            spLayer = layui.spLayer,
            spTable = layui.spTable;

        // 表格及数据初始化
        var tableIns = spTable.render({
            url: '${request.contextPath}/order/page',
            cols: [
                [{
                    type: 'checkbox'
                }, {
                    field: 'orderCode', title: '工单编号'
                }, {
                    field: 'bom', title: 'Bom编码'
                }, {
                    field: 'planQty', title: '计划数量'
                },   {
                    field: 'planStartTime', title: '计划开始时间 '
                },
                  /* {
                    field: 'planEndTime', title: '计划结束时间 '
                },*/
                    {
                    field: 'status', title: '订单状态 '
                }, {
                    field: 'flow', title: '工艺线路'
                }, {
                    field: 'makedQty', title: '已经生产'
                },
                    {
                        field: 'badQty', title: '不合格数量'
                    },
                    ,{
                    field: 'createTime', title: '创建时间 '
                },{
                    fixed: 'right',
                    field: 'operate',
                    title: '操作',
                    toolbar: '#js-record-table-toolbar-right',
                    unresize: true,
                    width: 150
                }]
            ],
            done: function (res, curr, count) {
            }
        });

        /*
         * 数据表格中form表单元素是动态插入,所以需要更新渲染下
         * http://www.layui.com/doc/modules/form.html#render
         */
        $(function () {
            form.render();
        });

        /**
         * 搜索按钮事件
         */
        form.on('submit(js-search-filter)', function (data) {
            //alert("product seaarch");
            //alert ("materiel="+data.field.productId);

           // var option=document.getElementById("js-status");
            //var  v =  option.options[option.options.selectedIndex].value;


            tableIns.reload({
                where: data.field,
                /*
                where : {
                    productId : $('#materiel').val(),
                    batchNo : $('#batchNo').val()
                },
               */

                /*where : {
                'orderCode':$("#orderCode").val(),
                'status':v
            },*/
                page: {
                    // 重新从第 1 页开始
                    curr: 1
                }
            });

            // 阻止表单跳转。如果需要表单跳转，去掉这段即可。
            return false;
        });



        /**
         * 头工具栏事件
         */
        table.on('toolbar(js-record-table-filter)', function (obj) {
            var checkStatus = table.checkStatus(obj.config.id);

            // 批量删除
            if (obj.event === 'deleteBatch') {
                var checkStatus = table.checkStatus('js-record-table');

                data = checkStatus.data;



                if (data.length > 0) {
                    var ids='';
                    //alert (Json.stringfy(data));
                    for (var item in data) {
                        ids += data[item].id + ",";
                    }
                    console.log(ids);//打印获取到选中的id，用，分割

                    layer.confirm('确认要删除吗？', function (index) {

                        spUtil.ajax({
                            url: '${request.contextPath}/order/batchDelete',
                            async: false,
                            type: 'POST',
                            // 是否显示 loading
                            showLoading: true,
                            // 是否序列化参数
                            serializable: false,
                            // 参数
                            data: {
                                ids: ids
                            },
                            success: function (data) {
                                tableIns.reload();
                                layer.close(index);
                            },
                            error: function () {
                            }
                        });

                    });
                } else {
                    layer.msg("请先选择需要删除的数据！");
                }
            }

            // 添加
            if (obj.event === 'add') {
                var index = spLayer.open({
                    title: '添加',
                    area: ['90%', '90%'],
                    content: '${request.contextPath}/order/add-or-update-ui'
                });
            }
        });

        /**
         * 监听行工具事件
         */
        table.on('tool(js-record-table-filter)', function (obj) {
            var data = obj.data;

            // 编辑
            if (obj.event === 'edit') {
                spLayer.open({
                    title: '编辑',
                    area: ['90%', '90%'],
                    // 请求url参数
                    spWhere: {id: data.id},
                    content: '${request.contextPath}/order/add-or-update-ui'
                });
            }

            // 删除
            if (obj.event === 'delete') {
                layer.confirm('确认要删除吗？', function (index) {
                    spUtil.ajax({
                        url: '${request.contextPath}/order/delete',
                        async: false,
                        type: 'POST',
                        // 是否显示 loading
                        showLoading: true,
                        // 是否序列化参数
                        serializable: false,
                        // 参数
                        data: {
                            id: data.id
                        },
                        success: function (data) {
                            tableIns.reload();
                            layer.close(index);
                        },
                        error: function () {
                        }
                    });
                });
            }
        });
    });

    function getvalue(obj)
    {
        alert(obj.options[obj.options.selectedIndex].value);
        var value=obj.options[obj.selectedIndex].value

        var text=obj.options[obj.selectedIndex].text
        alert("value="+value+"text="+text);
    }

    function getOrderStatusData() {
        spUtil.ajax({
            url: '${request.contextPath}/order/status2',
            async: false,
            type: 'GET',
            // 是否显示 loading
            // showLoading: true,
            // 是否序列化参数
            serializable: false,
            // 参数
            data: {},
            success: function (data) {
                $.each(data.data, function (index, item) {
                    //alert("index="+index+"item="+item);
                    $('#js-status').append(new Option(item, item));
                    //document.getElementById("js-status").append(new Option(item, item));
                    //document.getElementById("元素ID")与jquery中的$("#元素ID")是不等同的,应该用$("#元素ID").get(0)

                });
            }
        });

    }

    getOrderStatusData();

</script>
</body>
</html>